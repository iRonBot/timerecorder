﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Core {
  public partial class MainForm: Form {
    private static Boolean flag = true;
    private static Stopwatch stopwatch = new Stopwatch();
    private static TimeSpan timeSpan;

    public MainForm() {
      InitializeComponent();
    }

    private void doTask_Click(object sender, EventArgs e) {
      doTask.BeginInvoke(new MethodInvoker(() => {
        if (flag) {
          doTask.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
          doTask.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(83)))), ((int)(((byte)(80)))));
          doTask.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
          doTask.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
          doTask.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
          doTask.Text = "ادامه‌ی کار بعداً";
          flag = false;
          stopwatch.Reset();
          stopwatch.Start();
        }
        else {
          doTask.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
          doTask.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
          doTask.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
          doTask.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
          doTask.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
          doTask.Text = "شروع به کار";
          flag = true;
          stopwatch.Stop();
          TimeCollection.Items.Add(String.Format("{0:hh\\:mm\\:ss\\.ff}", stopwatch.Elapsed));
          timeSpan = new TimeSpan();
          foreach (String Line in TimeCollection.Items) {
            timeSpan += TimeSpan.Parse(Line);
          }
          TopGroupBox.Text = String.Format("[ کل کارکرد امروز: {0} ]", timeSpan);
        }
      }));
    }

    private void MainForm_Load(object sender, EventArgs e) {
      PersianCalendar persianCalendar = new PersianCalendar();
      int persianYear = persianCalendar.GetYear(DateTime.Now);
      int persianMonth = persianCalendar.GetMonth(DateTime.Now);
      int persianDay = persianCalendar.GetDayOfMonth(DateTime.Now);
      BottomGroupBox.Text = String.Format(BottomGroupBox.Text, String.Format("{0}/{1}/{2}", persianYear, persianMonth, persianDay));
    }

    private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
      using (StreamWriter streamWriter = new StreamWriter(String.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd-HHmmss")))) {
        foreach (String Line in TimeCollection.Items) {
          streamWriter.WriteLine(Line);
        }
        streamWriter.WriteLine(TopGroupBox.Text);
      }
    }
  }
}
