﻿namespace Core {
  partial class MainForm {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.TopGroupBox = new System.Windows.Forms.GroupBox();
      this.doTask = new System.Windows.Forms.Button();
      this.BottomGroupBox = new System.Windows.Forms.GroupBox();
      this.TimeCollection = new System.Windows.Forms.ListBox();
      this.TopGroupBox.SuspendLayout();
      this.BottomGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // TopGroupBox
      // 
      this.TopGroupBox.Controls.Add(this.doTask);
      this.TopGroupBox.Location = new System.Drawing.Point(8, 3);
      this.TopGroupBox.Name = "TopGroupBox";
      this.TopGroupBox.Size = new System.Drawing.Size(315, 100);
      this.TopGroupBox.TabIndex = 0;
      this.TopGroupBox.TabStop = false;
      this.TopGroupBox.Text = "[ کل کارکرد امروز: 00:00:00.00 ]";
      // 
      // doTask
      // 
      this.doTask.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
      this.doTask.Cursor = System.Windows.Forms.Cursors.Hand;
      this.doTask.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
      this.doTask.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
      this.doTask.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(248)))));
      this.doTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.doTask.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.doTask.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
      this.doTask.Location = new System.Drawing.Point(85, 31);
      this.doTask.Name = "doTask";
      this.doTask.Size = new System.Drawing.Size(150, 45);
      this.doTask.TabIndex = 0;
      this.doTask.Text = "شروع به کار";
      this.doTask.UseVisualStyleBackColor = false;
      this.doTask.Click += new System.EventHandler(this.doTask_Click);
      // 
      // BottomGroupBox
      // 
      this.BottomGroupBox.Controls.Add(this.TimeCollection);
      this.BottomGroupBox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.BottomGroupBox.Location = new System.Drawing.Point(8, 109);
      this.BottomGroupBox.Name = "BottomGroupBox";
      this.BottomGroupBox.Size = new System.Drawing.Size(315, 100);
      this.BottomGroupBox.TabIndex = 1;
      this.BottomGroupBox.TabStop = false;
      this.BottomGroupBox.Text = "[ مدت زمان کار در تاریخ {0} ]";
      // 
      // TimeCollection
      // 
      this.TimeCollection.FormattingEnabled = true;
      this.TimeCollection.ItemHeight = 14;
      this.TimeCollection.Location = new System.Drawing.Point(6, 21);
      this.TimeCollection.Name = "TimeCollection";
      this.TimeCollection.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.TimeCollection.Size = new System.Drawing.Size(303, 74);
      this.TimeCollection.TabIndex = 0;
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(331, 217);
      this.Controls.Add(this.BottomGroupBox);
      this.Controls.Add(this.TopGroupBox);
      this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.RightToLeftLayout = true;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "زمـان‌ســنـج";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.TopGroupBox.ResumeLayout(false);
      this.BottomGroupBox.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox TopGroupBox;
    private System.Windows.Forms.GroupBox BottomGroupBox;
    private System.Windows.Forms.Button doTask;
    private System.Windows.Forms.ListBox TimeCollection;
  }
}

